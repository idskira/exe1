from main import convert_to_10_base, convert_to_dst_base, build_base_set, convert_string, check_validation


def test_convert_number_to_10_base():
    assert convert_to_10_base('4B2', 16) == 1202
    assert convert_to_10_base('TTT', 36) == 38657
    assert convert_to_10_base('10', 10) == 10


def test_convert_number_to_dst_base():
    assert convert_to_dst_base(1202, 8) == '2262'
    assert convert_to_dst_base(215, 2) == '11010111'


def test_build_base_set():
    assert build_base_set(16) == ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    assert build_base_set(3) == ['0', '1', '2']


def test_validation():
    assert check_validation('DEADBEEF', 16) == True
    assert check_validation('DEADBEEF', 14) == False


def test_convertion():
    assert convert_string('DEADBEEF', 16, 10) == True
    assert convert_string('DEADBEEF', 2, 10) == False
