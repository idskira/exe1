import sys

FIRST_DIGIT = '0'
LAST_DIGIT = '9'
FIRST_CHARACTER = 'A'
LAST_CHARACTER = 'Z'


# This function convert the user string to decimal number
def convert_to_10_base(string_to_convert, src_base):
    base_characters = build_base_set(src_base)
    position = len(string_to_convert) - 1
    num_in_10_base = 0
    for letter in string_to_convert:
        num_in_10_base += (base_characters.index(letter) * pow(src_base, position))
        position -= 1
    return num_in_10_base


# This function convert the decimal number to number in the destination base
def convert_to_dst_base(num_in_10_base, dst_base):
    base_characters = build_base_set(dst_base)
    num_in_dst_base = ""

    while num_in_10_base > 0:
        digit = num_in_10_base % dst_base
        num_in_dst_base += base_characters[digit]
        num_in_10_base = int(num_in_10_base / dst_base)
    return num_in_dst_base[::-1]


# This function build the set of chars in the specific base, for example in base 2 the set is ['0', '1']
def build_base_set(src_base):
    base_characters = [chr(i) for i in range(ord(FIRST_DIGIT), ord(LAST_CHARACTER) + 1)
                       if (ord(chr(i)) < ord(':') or ord(chr(i)) > ord('@'))]
    base_characters = [base_characters[i] for i in range(src_base)]
    return base_characters


# This function check if all the characters in the user string is the specific base characters.
def check_validation(string_to_check, src_base):
    base_characters = build_base_set(src_base)
    for letter in string_to_check:
        if letter not in base_characters:
            print("'" + letter + "' not belong to base " + str(src_base))
            return False
    return True


# This function get the string that the user insert and the bases and return the converted number
# or False if the string is invalid.
def convert_string(string_to_convert, src_base, dst_base):
    string_to_convert = string_to_convert.upper()
    if not (check_validation(string_to_convert, src_base)):
        return False

    num_in_10_base = convert_to_10_base(string_to_convert, src_base)
    num_in_dst_base = convert_to_dst_base(num_in_10_base, dst_base)
    print("{} in base {} is: {}".format(string_to_convert, dst_base, num_in_dst_base))
    return True


def get_user_input(base_str):
    user_input = input("Please enter the {} base: ".format(base_str))
    if not (user_input.isdigit()) or (int(user_input) < 2) or (int(user_input) > 36):
        print("{} base must be a number between 2 to 36".format(base_str))
        return get_user_input(base_str)
    return int(user_input)


def main():
    user_string = input("Please enter your string:\n")
    # This while loops will run until the user insert valid source and destination bases

    src_base = get_user_input("source")
    dst_base = get_user_input("destination")

    if not (convert_string(user_string, src_base, dst_base)):
        print("This number: {} is not in base {}".format(user_string, src_base))
        return False
    return True


if __name__ == '__main__':
    main()
